#!/usr/bin/env python

import os
import requests
import argparse
import html
import re
from dotenv import load_dotenv

load_dotenv()


api_key = os.getenv("mmf_api_key")

if not api_key:
  print("Fill out your api key first")
  quit()

session = requests.Session()


def main():
  userFlag = args.user
  modelFlag = args.model
  categoryFlag = args.category

  if not modelFlag and not userFlag and not categoryFlag:
    if "/users/" in url:
      userFlag = True
    elif "/object/" in url:
      modelFlag = True
    elif "/category/":
      categoryFlag = True

  if modelFlag:
    model = model_from_url(url)
    if model:
      process_model(model)
  elif userFlag:
    user = user_from_url(url)
    if user:
      process_user(user)
  elif categoryFlag:
    category = category_from_url(url)
    if category:
      process_category(category)
  else:
    print("I'm not sure what to do.\n")
    parser.print_help()

  print("All Done")
  quit()


def sanitize_string(str):
  return re.sub('[^0-9a-zA-Z-._&\s]', '_', str)


def category_from_url(url):
  response = session.get(url)
  if not response.status_code == 200:
    print("Category not found.")
    return False

  return re.search('catid(\d*)', response.content.decode()).group(1)


def model_from_url(url):
  return url[url.rindex('-') + 1:]


def user_from_url(url):
  return url[url.rindex('/users/') + 7:]

def process_model(mid):
  next_url = "https://www.myminifactory.com/api/v2/objects/" + mid + "?key=" + api_key

  print("Trying to get model")

  data = fetch_results(next_url)

  if 'error' in data:
    print("Model not found")
    quit()

  if 'price' in data:
    print("Paid model found")
    quit()

  fetch_model(data)

  print('done')


def process_category(cid):
  per_page = 100
  next_url = "https://www.myminifactory.com/api/v2/search?key=" + api_key + "&cat=" + cid + "&sort=date&order=asc&per_page=" + str(per_page)
  print(next_url)
  page = 0

  while True:
    print("Trying to get models for category")

    data = fetch_results(next_url)
    print(data['total_count'])
    if not data['items']:
      print("No results for category")
      quit()

    for model in data['items']:
      if 'price' in model:
        continue
      fetch_model(model)

    page += 1
    if data['total_count'] >= page * per_page:
        next_url = "https://www.myminifactory.com/api/v2/search?key=" + api_key + "&cat=" + cid + "&sort=date&order=asc&per_page=" + str(per_page) + "&page=" + str(page)
    else:
      print('done')
      break


def process_user(user):
  per_page = 100
  next_url = "https://www.myminifactory.com/api/v2/users/" + user + "/objects?key=" + api_key + "&per_page=" + str(per_page)

  page = 0

  while True:
    print("Trying to get models for " + user)

    data = fetch_results(next_url)

    if not data['items']:
      print("No results for  " + user)
      quit()

    for model in data['items']:
      if 'price' in model:
        continue
      fetch_model(model)

    page += 1
    if data['total_count'] >= page * per_page:
      next_url = "https://www.myminifactory.com/api/v2/users/" + user + "/objects?key=" + api_key + "&per_page=" + str(per_page) + "&page=" + str(page)
    else:
      print('done')
      break


def create_dir(directory):
  if not os.path.exists(directory):
    print("Creating " + directory)
    os.makedirs(directory, exist_ok=True)


def fetch_results(next_url):
  try:
    r = session.get(next_url)
  except requests.exceptions.RequestException as e:
    print('An API error occured: {}'.format(e))
    quit()
  else:
    return r.json()


def fetch_file(url, model_dir, filename=''):

  model_dir = base_dir + os.path.sep + model_dir

  if not os.path.exists(model_dir + os.path.sep + filename):
    print("Fetching " + model_dir + os.path.sep + filename)
    model_file = session.get(url)

    if model_file.status_code is 200:
      create_dir(model_dir)
      open(model_dir + os.path.sep + filename, 'wb').write(model_file.content)
    else:
      print('Problem with request - Status code: ' + str(model_file.status_code))
  else:
    print(model_dir + os.path.sep + filename + ' already exists')


def fetch_model(model):
  user = sanitize_string(model['designer']['username'])
  model_name = sanitize_string(model['name'])

  model_dir = user + os.path.sep + model_name

  model_dir = model_dir

  thumbnail_url = model['images'][0]['original']['url']

  fetch_file(thumbnail_url, model_dir, filename="thumbnail.jpg")

  file_base_url = thumbnail_url[:thumbnail_url.rindex('images/')]

  files = model['files']['items']

  for file in files:
    file_url = file_base_url + '/threedfiles/' + file['filename']

    filename = sanitize_string(file['filename'])

    fetch_file(file_url, model_dir, filename=filename)


parser = argparse.ArgumentParser()
parser.add_argument('url', help="URL to parse")
parser.add_argument("--model", action="store_true", help="URL is a model")
parser.add_argument("--category", action="store_true", help="URL is a catrgory")
parser.add_argument("--user", action="store_true", help="URL is for a user")
parser.add_argument("-o", "--out", dest="dir", default="myminifactory", help="Directory to output files. Defaults to 'myminifactory'")

args = parser.parse_args()
base_dir = args.dir
url = args.url

main()
